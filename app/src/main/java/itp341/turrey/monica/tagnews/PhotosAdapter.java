package itp341.turrey.monica.tagnews;

/**
 * Created by monicaturrey on 12/12/15.
 */

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

import com.squareup.picasso.Picasso;


public class PhotosAdapter extends ArrayAdapter<Photo> {

    public PhotosAdapter(Context context, List<Photo> photos) {
        super(context, android.R.layout.simple_expandable_list_item_1, photos);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //get photo at position
      final Photo photo = getItem(position);


        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.image, null, false);
        }



        ImageView myImageView = (ImageView) convertView.findViewById(R.id.image);

        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        myImageView.getLayoutParams().height = displayMetrics.widthPixels;


        myImageView.setImageResource(0);
        // insert the image using picasso
        int ratio = photo.imageWidth / photo.imageHeight;
        int width = getContext().getResources().getDisplayMetrics().widthPixels;
        int height = width / Math.max(1, ratio);
        Picasso.with(getContext()).load(photo.imageUrl).resize(0, height).placeholder(R.drawable.placeholder).into(myImageView);


        return convertView;
    }
}

