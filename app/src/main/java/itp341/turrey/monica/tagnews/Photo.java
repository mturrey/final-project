package itp341.turrey.monica.tagnews;

import java.io.Serializable;

/**
 * Created by monicaturrey on 12/12/15.
 */
public class Photo implements Serializable {

        public String imageUrl;
        public int imageHeight;
        public int imageWidth;
}
